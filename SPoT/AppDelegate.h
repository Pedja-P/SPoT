//
//  AppDelegate.h
//  SPoT
//
//  Created by Predrag Pavlovic on 5/28/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
