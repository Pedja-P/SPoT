//
//  StanfordFlickrTagTVC.m
//  SPoT
//
//  Created by Predrag Pavlovic on 6/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "StanfordFlickrTagTVC.h"
#import "FlickrFetcher.h"

@interface StanfordFlickrTagTVC ()

@end

@implementation StanfordFlickrTagTVC

- (NSArray *)ignoredTags
{
    return @[@"cs193pspot", @"portrait", @"landscape"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.photos = [FlickrFetcher stanfordPhotos];
}

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/

@end
