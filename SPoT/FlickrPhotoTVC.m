//
//  FlickrPhotoTVC.m
//  Shutterbug
//
//  Created by CS193p Instructor.
//  Copyright (c) 2013 Stanford University. All rights reserved.
//

#import "FlickrPhotoTVC.h"
#import "FlickrFetcher.h"
#import "RecentPhotos.h"

@interface FlickrPhotoTVC ()

@end

@implementation FlickrPhotoTVC

// sets the Model
// reloads the UITableView (since Model is changing)
- (void)setFlickrPhotos:(NSArray *)flickrPhotos
{
    _flickrPhotos = [[flickrPhotos sortedArrayUsingDescriptors:[self sortDescriptors]] copy];
    [self.tableView reloadData];
}

- (NSArray *)sortDescriptors
{
    return nil;
}

- (NSString *)title
{
    if (!super.title) self.title = @"Photos"; // default title (it will be displayed in splitViewBarButtonItem)
    return super.title;
}

#pragma mark - Segue

// prepares for the "Show Image" segue by seeing if the destination view controller of the segue
// understands the method "setImageURL:"
// if it does, it sends setImageURL: to the destination view controller with
// the URL of the photo that was selected in the UITableView as the argument
// also sets the title of the destination view controller to the photo's title.
// Calls transferSplitViewBarButtonItemToViewController:
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"Show Image"]) {
                SEL setImageURLSelector = sel_registerName("setImageURL:"); // to suppress the warning "undeclared selector"
                [self transferSplitViewBarButtonItemToViewController:segue.destinationViewController];
                // we don't know the class (we haven't imported it), but we know it should respond to message
                if ([segue.destinationViewController respondsToSelector:setImageURLSelector]) {
                    NSURL *url = [FlickrFetcher urlForPhoto:self.flickrPhotos[indexPath.row] format:FlickrPhotoFormatLarge];
                    [segue.destinationViewController performSelector:setImageURLSelector withObject:url];
                    [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
                    [RecentPhotos addPhoto:self.flickrPhotos[indexPath.row]];
                }
            }
        }
    }
}

#pragma mark - UITableViewDataSource

// lets the UITableView know how many rows it should display
// in this case, just the count of dictionaries in the Model's array
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.flickrPhotos count];
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the title out of it
- (NSString *)titleForRow:(NSUInteger)row
{
    return [self.flickrPhotos[row][FLICKR_PHOTO_TITLE] description]; // description because flickr API
    // could return null (NSNull object) instead of string, description of null is @"[null]"
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the owner of the photo out of it
- (NSString *)subtitleForRow:(NSUInteger)row
{
    return [[self.flickrPhotos[row] valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description]; // description, because could be NSNull
}

// loads up a table view cell with the title and owner of the photo at the given row in the Model
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Flickr Photo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [self titleForRow:indexPath.row]; // this would crash if null was sent
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row]; // this too 
    
    return cell;
}

@end
